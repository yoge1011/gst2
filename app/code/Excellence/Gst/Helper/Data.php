<?php
/**
 * Copyright © 2015 Excellence . All rights reserved.
 */
namespace Excellence\Gst\Helper;

use Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

	/**
     * @param \Magento\Framework\App\Helper\Context $context
     */
	// public function __construct(\Magento\Framework\App\Helper\Context $context
	// ) {
	// 	parent::__construct($context);
	// }

	const XML_PATH_EXCELLENCE = 'excellence/';

	public function getConfigValue($field, $storeId = null)
	{
		return $this->scopeConfig->getValue(
			$field, ScopeInterface::SCOPE_STORE, $storeId
		);
	}

	public function getGeneralConfig($code, $storeId = null)
	{

		return $this->getConfigValue(self::XML_PATH_EXCELLENCE .'general/'. $code, $storeId);
	}
}